import json
import random

pokemon_data = {}

with open('data.json') as f:
  pokemon_data = json.load(f)

#~ for pokemon in pokemon_data:
  #~ print(pokemon_data[pokemon]['name'])
  #~ print(pokemon_data[pokemon]['moves'])
  #~ print(pokemon_data[pokemon]['stats'])
  #~ print('-----------------')

prop = input('Pick a stat for the fight: intelligence, strength, speed, attack, defense \n')

player1 = 'trump'
player2 = 'hillary'
p1move = random.choice(pokemon_data[player1]['moves'])
p2move = random.choice(pokemon_data[player2]['moves'])

print(pokemon_data[player1]['name'] + ' used ' + p1move)
print(pokemon_data[player2]['name'] + ' used ' + p2move)


if pokemon_data[player1]['stats'][prop] > pokemon_data[player2]['stats'][prop]:
  print(pokemon_data[player1]['name'] + ' wins!')
elif pokemon_data[player1]['stats'][prop] < pokemon_data[player2]['stats'][prop]:
  print(pokemon_data[player2]['name'] + ' wins!')
else:
  print('America wins! They both lose!')
