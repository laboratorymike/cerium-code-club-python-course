************************************************************************
*****                        LESSON 5 README                       *****
************************************************************************

*** Installations
- Python Requests

*** Directions
For this activity, we are calling the LaboratoryMike.com English Words 
API, in order to build Wildcard Story cards, and to create our own 
word-based game.

The included "labmike_api.py" has a shell to get you started, with a 
login and logout script. Copy this to "my_api.py" and edit this file.

Try querying the "node endpoint" for the "word" and "word_definition" 
content type.
