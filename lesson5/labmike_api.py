import csv
import json
import requests
from time import sleep

# Log In

domain = 'http://www.laboratorymike.com/'

login_endpoint = 'language-upload/user/login.json'
logout_endpoint = 'language-upload/user/logout.json'
node_endpoint = 'language-upload/node'
node_entity_endpoint = 'language-upload/entity_node'

login_data = {'username': '', 'password': ''}
ld =json.dumps(login_data)

header = {'Content-Type':'application/json'}

try:
  l = requests.post(domain + login_endpoint, headers=header, data=ld)
  user_data = json.loads(l.text)
  settings['header']['Cookie'] = user_data['session_name'] + '=' + user_data['sessid']
  settings['header']['X-CSRF-Token'] = user_data['token']
  print('Login successful.')
except:
  print('Login failed.')

# Read the file



# logout

requests.post(domain + logout_endpoint, headers=header)
