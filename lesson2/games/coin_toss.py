import random
from time import sleep
from .common import pprint

class CoinToss():

  def play(self):
    ### We need a way to keep score - Define your scoring variable(s) here:
    
    ### Print your welcome message here. Use the pprint function
    pprint('')
    ### We need a variable called 'running' to determine if the game should keep running
    
    ### Let's create a 'while' loop that will run as long as the game 
    # status is set to 'running'
    
      ### Ask the person if they want to pick heads or tails
      
      ### Let's make the choice lowercase, so we don't have to think 
      # about capitalization
      
      ### Now, let the computer pick heads or tails
      
      ### The player and the computer have both made a choice. Let's
      # see if the player wins. If he does, give him a point. Otherwise,
      # count it as a miss.
      
      # Print out the score so far...
      
      ### If we do not have a way to stop the game, it will run forever.
      # Ask the player at this point if they would like to keep going.
      
