import random
from time import sleep
from .common import pprint

class RockPaperScissors():
  running = True
  score = {'Computer':0,'Player':0}
  def play(self):
    pprint('This is rock, paper, scissors')
    while self.running:
      user_choice = 'a'
      while user_choice not in ['rock', 'paper', 'scissors']:
        user_choice = input("Choose 'rock', 'paper', or 'scissors'\n")
      pprint('You chose ' + user_choice)
      comp_choice = random.choice(['rock', 'paper', 'scissors'])
      pprint('Computer chose ' + comp_choice)
      winner = ''
      if user_choice == 'rock':
        if comp_choice == 'rock':
          pass
        elif comp_choice == 'paper':
          winner = 'Computer'
        elif comp_choice == 'scissors':
          winner = 'Player'
      elif user_choice == 'paper':
        if comp_choice == 'rock':
          winner = 'Player'
        elif comp_choice == 'paper':
          pass
        elif comp_choice == 'scissors':
          winner = 'Computer'
      elif user_choice == 'scissors':
        if comp_choice == 'rock':
          winner = 'Computer'
        elif comp_choice == 'paper':
          winner = 'Player'
        elif comp_choice == 'scissors':
          pass
      if not winner:
        pprint('Tie!')
      if winner:
        pprint(winner + ' won!')
        self.score[winner] += 1
      
      pprint('Computer: ' + str(self.score['Computer']) + ', Player: ' + str(self.score['Player']))
      sleep(1)
      play_again = input('Play Another Game? (y/n)\n')
      if play_again == 'n':
        self.running = False
