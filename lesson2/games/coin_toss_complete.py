import random
from time import sleep
from .common import pprint

class CoinToss():

  score = {'Correct':0,'Missed':0}

  def play(self):
    ### Print your welcome message here:
    pprint("*** Let's play Coin Toss! ***")
    ### We need a variable called 'running' to determine if the game
    # should keep running
    running = True
    ### Let's create a 'while' loop that will run as long as the game 
    # status is set to 'running'
    while running:
      ### Ask the person if they want to pick heads or tails
      choice = input('Call it - heads or tails?\n')
      ### Let's make the choice lowercase, so we don't have to think 
      # about capitalization
      choice = choice.lower()
      ### Now, let the computer pick heads or tails
      comp_choice = random.choice(['heads','tails'])
      pprint('And the coin toss is... ' + comp_choice)
      sleep(1)
      ### The player and the computer have both made a choice. Let's
      # see if the player wins. If he does, give him a point. Otherwise,
      # count it as a miss.
      if choice == comp_choice:
        pprint('You win the toss!')
        self.score['Correct'] += 1
      else:
        pprint('You lose!')
        self.score['Missed'] += 1
      sleep(0.5)
      # Print out the score so far...
      pprint('Your Score:')
      pprint('Correct: ' + str(self.score['Correct']))
      pprint('Missed: ' + str(self.score['Missed']))

      ### If we do not have a way to stop the game, it will run forever.
      # Ask the player at this point if they would like to keep going.
      play_again = input('Play Again? (y/n)\n')
      if play_again == 'n':
        running = False
