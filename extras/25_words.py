from time import sleep
import json
import random
import sys

# Utility function
def pprint(text):
  sys.stdout.write(str(text) + '\n')
  sys.stdout.flush()

# Data for game
allowed_lengths = ['10', '15', '20', '25']

words = []
players = {}

with open('words.json', 'r') as f:
  words = json.load(f)
with open('players.json', 'r') as f:
  players = json.load(f)

running = True

while running:
  # Shuffle the player/question list
  random.shuffle(players)
  random.shuffle(words)
  i = 0
  j = 0
  while i < (len(players) - 1) and j < (len(words) - 1):
    player_1 = players[i]
    i += 1
    player_2 = players[i]
    i += 1
    question = words[j]
    j += 1
    length = random.choice(allowed_lengths)
    pprint('Our contestants: ' + player_1['name'] + ' (1) and ' + player_2['name'] + ' (2)')
    go = input('Press Enter to start')
    pprint('The question is "' + question + '." you have 15 seconds and ' + length + ' words!')
    sleep(30)
    pprint("TIME'S UP !")
    winner = ''
    while winner not in ['1', '2']:
      winner = input('Who won? player 1 or 2?\n')
    if winner == '1':
      player_1['score'] += 1
    else:
      player_2['score'] += 1
    winner = ''
    pprint('--- Current scores ---')
    for player in players:
      pprint(player['name'] + ' - ' + str(player['score']))
  # Game is done. See if people want to keep playing
  pprint('*************************************************************')
  pprint('********************    Game Finished    ********************')
  pprint('*************************************************************')
  play_again = input('Play Another Game? (y/n)\n')
  if play_again == 'n':
    running = False
