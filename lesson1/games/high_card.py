import random
from time import sleep
from .common import pprint

class HighCard():
  card_suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
  cards = {}
  cards['2'] = 2
  cards['3'] = 3
  cards['4'] = 4
  cards['5'] = 5
  cards['6'] = 6
  cards['7'] = 7
  cards['8'] = 8
  cards['9'] = 9
  cards['10'] = 10
  cards['Jack'] = 11
  cards['Queen'] = 12
  cards['King'] = 13
  cards['Ace'] = 14
  card_keys = list(cards.keys())

  running = True

  score = {'Computer':0,'Player':0}

  def play(self):
    pprint('*** Welcome to High Card! ***')
    while self.running:
      # draw cards
      comp_card = random.choice(self.card_keys)
      comp_card_suit = random.choice(self.card_suits)
      pprint('The computer drew ' + comp_card + ' of ' + comp_card_suit)
      sleep(0.5)
      player_card = random.choice(self.card_keys)
      player_card_suit = random.choice(self.card_suits)
      pprint('You drew ' + player_card + ' of ' + player_card_suit)
      sleep(0.5)

      # compare cards
      if self.cards[comp_card] > self.cards[player_card]:
        pprint("Computer wins!")
        self.score['Computer'] += 1
      elif self.cards[comp_card] < self.cards[player_card]:
        pprint("You win!")
        self.score['Player'] += 1
      else:
        pprint("It's a tie!")
      sleep(0.5)

      # Print score and play again
      pprint('** Current Score **')
      pprint('Computer: ' + str(self.score['Computer']))
      pprint('Player: ' + str(self.score['Player']))
      pprint('\n')
      play_again = input('Play Again? (y/n)\n')
      if play_again == 'n':
        self.running = False
