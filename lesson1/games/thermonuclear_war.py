from time import sleep
from .common import pprint

class ThermonuclearWar():
  def play(self):
    pprint('A strange game... the only winning move is not to play.')
    sleep(2)
