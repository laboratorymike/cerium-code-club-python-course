from time import sleep
from games.common import pprint
from games.high_card import HighCard

## We need to import our games! Look at the available game in the games 
## folder, and add them like the way we added 'War' above.


## ^^ Add in this space ^^

# Game List
game_list = {}
game_list['HighCard'] = HighCard()

## We need to add our games to the game list! Add them like the way we 
## added 'War' above. Don't forget the parentheses: ()


## ^^ Add in this space ^^


########################################################################
# We will learn more about game making in future lessons. For now,
# please leave the code below alone.
########################################################################

# Game machine parameters
program_running = True

while program_running:
  # Welcome Message
  pprint('\n***** Welcome to the Game machine! *****\n')
  sleep(1)
  # Select game
  pprint('These are the games available:\n')
  for game in game_list:
    pprint(game)
  chosen_game = input('\nEnter the name of the game you would like to play:\n')
  while chosen_game not in game_list:
    pprint('Sorry, that is not the name of a valid game.')
    chosen_game = input('\nEnter the name of the game you would like to play:\n')
  # Play game
  game_list[chosen_game].play()
  # Ask about the next game
  pprint('\n\n\n\n***** Game Finished! *****')
  sleep(1)
  play_again = input('Play Another Game? (y/n)\n')
  if play_again == 'n':
    program_running = False
